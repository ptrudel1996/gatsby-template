import React from 'react';
import Navbar from './navbar';
import Footer from './footer';
//import 'bulma';
import '../styles/index.scss';

const Layout = ({ children }) => {
  return (
    <>
      <main>{children}</main>
      <Footer />
    </>
  );
};

export default Layout;

import { Link } from 'gatsby';
import React from 'react';
import Navbar from './navbar';

const Header = () => {
  return (
    <section className="hero is-fullheight">
      <div className="hero-head">
        <Navbar />
      </div>

      <div className="hero-body">
        <div className="container has-text-centered">
          <h1 className="title has-text-white is-size-1">Template title</h1>
          <h2 className="subtitle has-text-white">Template subtitle</h2>
        </div>
      </div>
    </section>
  );
};

export default Header;

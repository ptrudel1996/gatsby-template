import React from 'react';
import { Link } from 'gatsby';
import Layout from '../components/layout';
import Header from '../components/header'
import SEO from '../components/seo';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Header />
    <div className="is-fullheight" >
      kek
    </div>
    <h1>Home</h1>
  </Layout>
);

export default IndexPage;
